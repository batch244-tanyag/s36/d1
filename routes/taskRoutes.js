// routes- contain all the end point for our application

const express = require("express");
const router = express.Router()

const taskController = require("../controllers/taskcontroller")

// route to all get the task

router.get("/", (req, res) => {
	taskController.getAllTasks() .then(resultFromController => 
		res.send(resultFromController));
});

// route to create a new task

router.post("/", (req, res) => {
	taskController.createTask(req.body) .then(resultFromController => 
		res.send(resultFromController));
});



// router to delete a task
// ":id" is a wildcard
router.delete("/:id", (req, res) => {
	taskController.deleteTask(req.params.id).then(resultFromController => res.send(resultFromController));
		// params - URL parameter
})


// router to update a task

router.put("/:id", (req, res) => {
	taskController.updateTask(req.params.id, req.body).then(resultFromController => res.send(resultFromController));
})




// router to get a specific task

router.get("/:id", (req, res) => {
	taskController.getTask(req.params.id).then(resultFromController => res.send(resultFromController));
		
})


// router to update a specific task status

router.put("/:id/complete", (req, res) => {
	taskController.updateTaskStatus(req.params.id, {"status":"complete"}).then(resultFromController => res.send(resultFromController));
})




// export the module
module.exports = router;

