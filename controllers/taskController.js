// controllers- business logic
const Task = require("../models/task");

// controller for getting all tasks

module.exports.getAllTasks = () => {
	return Task.find({}).then(result => {
		return result;
	});
};

// creating tasks

module.exports.createTask = (requestBody) => {
	let newTask = new Task({
		name: requestBody.name
	})
	return newTask.save().then((task, error)=> {
		if(error){
			console.log(error);
			return false;
		} else {
			return task;
		}
	})
}

// deleting task



module.exports.deleteTask = (taskId) => {

	return Task.findByIdAndRemove(taskId).then((removedTask, err) => {
		if(err){
			console.log(err);
			return false;
		} else {
			return removedTask;
		}
	})
}



// updating task

module.exports.updateTask = (taskId, newContent) => {
	return Task.findById(taskId).then((result, error)=> {
		if (error){
			console.log(error);

		}else {
			result.name =newContent.name;
			return result.save().then((updatedTask, saveErr)=> {
				if(saveErr) {
					console.log(saveErr)
					return false
				}else {
					return updatedTask
				}
			})
		}
	})
}



// get a specific task



module.exports.getTask = (taskId) => {
	return Task.find({taskId}).then(result => {
		return result;
	});
};




// updating task

module.exports.updateTaskStatus = (taskId, newContent) => {
	return Task.findById(taskId).then((result, error)=> {
		if (error){
			console.log(error);

		}else {
			result.status =newContent.status;
			return result.save().then((updatedTask, saveErr)=> {
				if(saveErr) {
					console.log(saveErr)
					return false
				}else {
					return updatedTask
				}
			})
		}
	})
}